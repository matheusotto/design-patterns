package com.exemplos.factory;

import javax.xml.parsers.DocumentBuilderFactory;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Stream;

public class FactoryMethods {
    // virtual constructor
    public static void main(String[] args) {
        // Static Factory Methods
        Calendar.getInstance();
        NumberFormat.getInstance();
        Stream.of(1, 2, 3, 4);
        List.of(1, 2, 3, 4);
        Integer.valueOf("1234");
    }

    class FactoryMethod_1 {

        // Criador Abstrato
        // Produto Abstrato
        // Criador Concreto
        // Produto Concreto

        @SuppressWarnings("unused")
        public void main(String[] args) {
            Categoria2 categoria2 = new Categoria2();
            Produto2 produto2 = categoria2.novoProduto();

        }

    }
}

interface Produto2 { // Produto Abstrato
}

class ProdutoDigital2 implements Produto2 {
}

class ProdutoFisico2 implements Produto2 {
}

class Categoria2 {

    // A lógica fica dentro da classe criadora. Não da classe produto em si.

//    private String nome;
//    private Tipo tipo;
//    private int quantidade;

    public Produto2 novoProduto() {
        //....
        return new ProdutoFisico2();
    }
}

class CategoriaDigital extends Categoria2 {
    public Produto2 novoProduto() {
        //...
        return new ProdutoDigital2();
    }
}


// Format 3

class FactoryMethod_3 {
    // Criador concreto com parâmetro
    // Produto Abstrato
    // Produto Concreto

    @SuppressWarnings("unused")
    public static void main(String[] args) {
        Categoria3 categoria3 = new Categoria3();
        Produto3 produto3 = categoria3.novoProduto(1);
    }
}

interface Produto3 { // Produto Abstrato
}

// Produtos Concretos

class ProdutoPadrao3 implements Produto3 {
}

class ProdutoDigital3 implements Produto3 {
}

class ProdutoFisico3 implements Produto3 {
}

class Categoria3 {
    private String nome;
    private Integer prioridade;

    //...


    //Fabrica dos produtos
    public Produto3 novoProduto(int tipoProduto) {
        switch (tipoProduto) {
            case 1:
                return new ProdutoPadrao3();
            case 2:
                return new ProdutoDigital3();
            case 3:
                return new ProdutoFisico3();
            default:
                throw new IllegalArgumentException();
        }
    }
}


// O mais usado, comum

interface Produto {
}

class ProdutoDitigal implements Produto {
}

class ProdutoFisico implements Produto {
}

interface Categoria { // CLASSE OU INTERFACE ABSTRATA QUE DEFINE O MÉTODO QUE CRIA INSTANCES DO PRODUTO
    Produto novoProduto();  // FACTORY METHOD
}

// CRIADORES ALTERNADOS

class Digital implements Categoria {

    // Pode ter atributos
    @Override
    public Produto novoProduto() {
        // Pode ter lógica
        return new ProdutoDitigal();
    }
}

class Fisico implements Categoria {

    @Override
    public Produto novoProduto() {
        return new ProdutoFisico();
    }
}

// EXEMPLO DE UMA CLASSE FACTORY DO JAVA

class TesteExemplo {
    public static void main(String[] args) {
        DocumentBuilderFactory.newInstance();
    }
}
