package com.exemplos.strategy;

import java.math.BigDecimal;

//DADOS
public class Strategy {

    // Estrategia - Interface
    // Estrategia - Concreta
    // Contexto

    public static void main(String[] args) {
        BigDecimal valor = new BigDecimal("10");
        Compra compra = new Compra(valor);

        compra.processarCompra(new PagamentoCartaoDebito());
//        compra.processarCompra(new PagamentoCartaoCredito());
    }


}

class Compra { // Contexto

    BigDecimal valor;

    public Compra(BigDecimal valor) {
        this.valor = valor;
    }

    void processarCompra(EstrategiaPagamento estrategiaPagamento) {
        estrategiaPagamento.pagar(valor);
    }
}

// PagamentoStrategy --> PRINCIPAL VANTAGEM É PODER ENCAPSULAR COMPORTAMENTOS
interface EstrategiaPagamento { // Estrategia - Interface

    void pagar(BigDecimal valor);
}

class PagamentoCartaoCredito implements EstrategiaPagamento { // Estratégia - Concreta

    @Override
    public void pagar(BigDecimal valor) {
        System.out.println("Pagou no Crédito " + valor);
    }
}

class PagamentoCartaoDebito implements EstrategiaPagamento {

    @Override
    public void pagar(BigDecimal valor) {
        System.out.println("Pagou no Débito " + valor);
    }
}


// Instância
class Strategy_2 {
    public static void main(String[] args) {
        BigDecimal valor = new BigDecimal("10");
        Compra2 compra = new Compra2(valor);

        compra.processarCompra(new PagamentoCartaoDebito2());
//        compra.processarCompra(new PagamentoCartaoCredito2());
    }
}

class Compra2 {
    //Long id;
    BigDecimal valor;
    // Tipo tipo;
    public Compra2(BigDecimal valor) {
        this.valor = valor;
    }

    void processarCompra(EstrategiaPagamento2 estrategiaPagamento2) {
        estrategiaPagamento2.pagar(this);
    }
}


interface EstrategiaPagamento2 { // Recebe Objeto - Precisa reconhecer a classe compra.
    void pagar(Compra2 compra);
}

class PagamentoCartaoCredito2 implements EstrategiaPagamento2 {

    @Override
    public void pagar(Compra2 compra) {
        System.out.println("Pagou no Crédito " + compra.valor);
    }
}

class PagamentoCartaoDebito2 implements EstrategiaPagamento2 {

    @Override
    public void pagar(Compra2 compra) {
        System.out.println("Pagou no Débito " + compra.valor);
    }
}


// ------------------------------- Exemplo Instance por interface

interface Pagavel{
    BigDecimal getValor();
}

class Compra3 implements Pagavel {

    BigDecimal valor;

    public Compra3(BigDecimal valor){
        this.valor = valor;
    }

    public void processar(EstrategiaPagamento3 estrategiaPagamento3) {
        estrategiaPagamento3.pagar(this);
    }

    @Override
    public BigDecimal getValor() {
        return valor;
    }
}

interface EstrategiaPagamento3 {
    void pagar(Pagavel compra);
}

class PagamentoCartaoCredito3 implements EstrategiaPagamento3 {
    @Override
    public void pagar(Pagavel pagavel) {
        System.out.println("Pagou no Crédito " + pagavel.getValor());
    }
}

class PagamentoCartaoDebito3 implements EstrategiaPagamento3 { // Não tem mais conhecimento  da implementação concreta
    @Override                                                  // Vai ter conhecimento apenas da interface Pagavel
    public void pagar(Pagavel pagavel) {
        System.out.println("Pagou no Débito " + pagavel.getValor());
    }
}


// ------------- Lambda

class Strategy_4 {
    public static void main(String[] args) {

//        List<Cliente> listaClientes;
//        listaClientes.sort((c1,c2) -> c1.id > c2.id))

        BigDecimal valor = new BigDecimal("10");
        Compra4 compra = new Compra4(valor);

        compra.processarCompra(v-> System.out.println("Pagou no Crédito" + v));
    }
}

class Compra4 {
    BigDecimal valor;

    public Compra4(BigDecimal valor) {
        this.valor = valor;
    }

    void processarCompra(EstrategiaPagamento4 estrategiaPagamento4){
        estrategiaPagamento4.pagar(valor);
    }
}

interface EstrategiaPagamento4{
    void pagar(BigDecimal valor);
}