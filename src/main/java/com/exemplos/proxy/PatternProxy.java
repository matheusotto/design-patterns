package com.exemplos.proxy;

import java.util.HashMap;

public class PatternProxy {

    public static void main(String[] args) {
        ProdutoDAO produtoDAO = new LogProdutoDAO();
        ProdutoService produtoService = new ProdutoService(produtoDAO);

        produtoService.buscarProduto(3);
        produtoService.buscarProduto(2);
        produtoService.buscarProduto(1);
    }
}

class ProdutoService {
    private ProdutoDAO produtoDAO;

    public ProdutoService(ProdutoDAO produtoDAO) {
        this.produtoDAO = produtoDAO;
    }

    public Object buscarProduto(long id) {
        return produtoDAO.find(id);
    }
}

class ProdutoDAO {
    public ProdutoDAO() {
        simulaTempoAlto();
    }

    public Object find(long id) {
        simulaTempoAlto();
        return new Object();
    }

    private void simulaTempoAlto() {
        try {
            // Cria conexão com o banco
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

// Não precisa mexer no cliente
// Não mexi na implementação Original
// Usei Herança que é uma das formas de fazer proxy

class LogProdutoDAO extends ProdutoDAO { // Estendi a classe e adicionei a funcionalidade que queria

    @Override
    public Object find(long id) {
        System.out.println("Buscando produto com Id " + id);
        return super.find(id);
    }

}


// EXEMPLO 2

class Proxy_2 {
    // RealSubject / Implementação
    // Proxy

    public static void main(String[] args) {
        //ProdutoDAO2 produtoDAO2 = new ProdutoDAO2();
        ProdutoDAO2 produtoDAO2 = new CacheProdutoDAO2();
        ProdutoService2 produtoService2 = new ProdutoService2(produtoDAO2);

        produtoService2.buscarProduto(1);
        produtoService2.buscarProduto(1);
        produtoService2.buscarProduto(1);
    }
}

class ProdutoDAO2 {
    public ProdutoDAO2() {
        simulaTempoAlto();
    }

    public Object find(long id) {
        System.out.println("Busca o objeto na Base.");
        simulaTempoAlto();
        return new Object();
    }

    private void simulaTempoAlto() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class CacheProdutoDAO2 extends ProdutoDAO2 {

    private static final HashMap<Long, Object> CACHE = new HashMap<>(); //CACHE LOCAL (exemplo apenas)

    @Override
    public Object find(long id) {
        Object o = CACHE.get(id);
        if (o == null) {
            o = super.find(id);
            CACHE.put(id, o);
            return o;
        }
        System.out.println("Busca o Objeto no Cache");
        return o;
    }
}

class ProdutoService2 {
    private ProdutoDAO2 produtoDAO2;

    public ProdutoService2(ProdutoDAO2 produtoDAO2) {
        this.produtoDAO2 = produtoDAO2;
    }

    public Object buscarProduto(long id) {
        return produtoDAO2.find(id);
    }
}


class Proxy_3_Lazy {

    // REALSUBJECT / IMPLEMENTAÇÃO
    // PROXY - LAZYPRODUTODAO3
    // SUBJECT - INTERFACE

    public static void main(String[] args) {
        //ProdutoDAO2 produtoDAO2 = new ProdutoDAO2();
        LazyProdutoDAO3 produtoDAO3 = new LazyProdutoDAO3();
        ProdutoService3 produtoService3 = new ProdutoService3(produtoDAO3);

        System.out.println("Produto Service Criado");
        produtoService3.buscarProduto(1);
        produtoService3.buscarProduto(2);
        produtoService3.buscarProduto(3);
    }
}

class ProdutoService3 {

    private IProdutoDAO3 iProdutoDAO3;

    public ProdutoService3(IProdutoDAO3 iProdutoDAO3) {
        this.iProdutoDAO3 = iProdutoDAO3;
    }

    public Object buscarProduto(long id){
        return iProdutoDAO3.find(id);
    }
}

interface IProdutoDAO3 {
    Object find(long id);
}

class ProdutoDAO3 implements IProdutoDAO3 {

    public ProdutoDAO3() {
        System.out.println("Criando Conexão com o Banco");
        simulaTempoAlto();
    }

    @Override
    public Object find(long id) {
        simulaTempoAlto();
        return new Object();
    }

    private void simulaTempoAlto() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


/// PROXY

// Usa composição ao invés de herança

// Escolher entre composição(interface) ou herança

// Composição implementa uma interface, e armazena o atributo do proxy quem é a Implementação REAL.

class LazyProdutoDAO3 implements IProdutoDAO3 {

    private IProdutoDAO3 daoReal;

    @Override
    public Object find(long id){
        inicializaDAO();
        return daoReal.find(id);
    }

    private void inicializaDAO() {
        if (daoReal == null) {
            daoReal = new ProdutoDAO3();
        }
    }
}

// Exmplos Reais = @Inject, @AutoWired, @EJB ( geralmente quando injeta instances, é uma proxy(subclasse) do produto )
// RMI // java.rmi - remote method invocation (faz chamadas em outras jvms) geralmente utiliza proxy