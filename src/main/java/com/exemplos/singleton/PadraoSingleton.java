package com.exemplos.singleton;

public class PadraoSingleton {

    public static void main(String[] args) {
//        ProdutoFactory.INSTANCE.novoProduto(1);
//        ProdutoFactory.INSTANCE.novoProduto(3);
//        ProdutoFactory.INSTANCE.novoProduto(2);

        ProdutoFactory.getInstance().novoProduto(1);
        ProdutoFactory.getInstance().novoProduto(3);
        ProdutoFactory.getInstance().novoProduto(2);

        // ENUN

        ProdutoFactory2.INSTANCE.novoProduto(1); //Opção Recomendada para Singleton - > ENUN.
    }

}

class ProdutoFactory {

//    public static final ProdutoFactory INSTANCE = new ProdutoFactory();
    public static ProdutoFactory instance; // Metodo Lazy

    private ProdutoFactory() {
    }

    public synchronized static ProdutoFactory getInstance(){ // não é multithread - utilizar syncronized pra não ocorrer paralelismo
        if (instance == null){
            instance = new ProdutoFactory();
        }
        return instance;
    }

    public Produto4 novoProduto(int tipoProduto) {
        switch (tipoProduto) {
            case 1:
                return new ProdutoPadrao4();
            case 2:
                return new ProdutoDigital4();
            case 3:
                return new ProdutoFisico4();
            default:
                throw new IllegalArgumentException();
        }
    }
}

interface Produto4 {
}

class ProdutoPadrao4 implements Produto4 {
}

class ProdutoDigital4 extends ProdutoPadrao4 {
}

class ProdutoFisico4 extends ProdutoPadrao4 {
}


// --- SINGLETON ENUN

enum ProdutoFactory2 {
    INSTANCE;

    public Produto4 novoProduto(int tipoProduto){
        switch (tipoProduto) {
            case 1:
                return new ProdutoPadrao4();
            case 2:
                return new ProdutoDigital4();
            case 3:
                return new ProdutoFisico4();
            default:
                throw new IllegalArgumentException();
        }
    }
}